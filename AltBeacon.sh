#!/bin/bash

# FILE MODIFIED: Program made more dynamic, additional comments describing functionality of program

# 1.1.1:
# - Un-hardcoded variables (Floor, Class)
# - Clearer comments/documentation for the commands
# - Blocked booting Bluetooth command with invalid setup parameters

# Example command: 
# sh AltBeacon-v110.sh -c 305 -f B3 -u "38 32 07 7d d2 5f 4f e9 a4 2a 8a a0 08 75 f4 bb" -l 2

# Referenced during development:
# https://github.com/RadiusNetworks/altbeacon-reference/blob/master/altbeacon_transmit

# Sleep command necessary to accomodate for proper systemd initialisation on Raspberry Pi hardware
sleep 10
SETUP='false'

Help()
{
	echo "AltBeacon script - Options:"
	echo
	echo "Usage: altbeacon [-f -c -u -l|h]"
	echo "f:	floor (as hexadecimal code, with the letter representing the wing/sector of the building, and the number representing the actual floor. For example: B3)"
	echo "c:	class (as a regular number, for example: 305)"
	echo "u:	UUIDv4 (split up in 2 bytes, with the dashes removed, and in quotation marks for example: "38 32 07 7d d2 5f 4f ... ... bb")"
	echo "l:	location (as a regular number between 1 and 3; 1 = Sittard, 2 = Maastricht, 3 = Heerlen)"
	echo "h:	Shows instructions of this script"
}

# Options processing
while getopts "hc:f:u:l:" option; do
	case ${option} in
		h) Help ;;
		
		
		c) SETUP="true"
		class=${OPTARG}
		echo "Class: " $class
		HEX_CLASS=$( printf "%x" $((${class})) )
		
		# Adding additional zeroes, taking empty space of byte into account
		if [ ${#HEX_CLASS} = "3" ]; then HEX_CLASS=0$HEX_CLASS
		elif [ ${#HEX_CLASS} = "2" ]; then HEX_CLASS=00$HEX_CLASS
		elif [ ${#HEX_CLASS} = "1" ]; then HEX_CLASS=000$HEX_CLASS
		fi
		
		echo "Class, converted to Hex / Zuyd formatting: " $class
		
		# Splits into pairs of 2 bytes
		HEX_CLASS=`echo $HEX_CLASS | sed 's/.\{2\}/& /g'`;;
		
		
		f) SETUP="true"
		echo "Floor: " ${OPTARG}
		floor=${OPTARG}
		floor=`echo $floor | tr '[:upper:]' '[:lower:]'`;;
		#echo $floor;;
		u) SETUP="true" 
		echo "UUID: " ${OPTARG}
		uuid=${OPTARG};;
		l) SETUP="true"
		LOCATION=${OPTARG}
		
		if [ $LOCATION = "1" ]; then
		 echo "Location: Sittard, " ${OPTARG}
		elif [ $LOCATION = "2" ]; then
		 echo "Location: Heerlen, " ${OPTARG}
		elif [ $LOCATION = "3" ]; then
			echo "Location: Maastricht, " ${OPTARG}
		else
			echo "Invalid location"
		fi
		
		echo "Location: "  ${OPTARG};;
		
		
		\?) echo "Invalid option" ;;
	esac
done

if [ SETUP="true" ]
then
	if [ -z "$class" ] || [ -z "$floor" ] || [ -z "$uuid" ] || [ -z "$LOCATION" ] 
	then
		echo "Missing variables. Make sure to fill out every bit of asked information, when setting up your beacon."
	else
		# Enabling Bluetooth
		sudo hciconfig hci0 up
		# Stopping any advertising
		sudo hciconfig hci0 noleadv
		# Setting advertising
		sudo hciconfig hci0 leadv 3
		# Disabling classic Bluetooth visibility
		sudo hciconfig hci0 noscan
		
		
		# Bluetooth command for activating AltBeacon
		
		# Static components:
		# - Bluetooth commands (0x08 up to 02)
		# - Advertisement Flag structure (02 up to 1b)
		# - AltBeacon advertising length of all the data [27 bytes] (1b) 
		# - Manufacturer specfic code (ff)
		# - Manufacturer SIG Bluetooth Member code [FF FF/65535 is considered standard for non-members/internal testing] (ff ff)
		# - AltBeacon/Beacon type code (be ac)
		# - Additional zeroes, to pad out empty bytes (00)
		# - RSSI reference [for calculating distance] (c5)
		# - Additional zero for location number, originally used as special feature byte for manufacturer features
		
		# Dynamic components:
		# - UUID (version 4); used tool for generating random UUID: https://www.uuidgenerator.net/version4
		# - Floor, as in hex code: letter+number combo, to point out section/wing of building and floor number (B2, C3, D4 for example)
		# - Class, converted from number to hex code with added zeroes to pad out the empty bytespace
		# - Location numbed, 1 = Sittard, 2 = Heerlen, 3 = Maastricht based on Zuyd Hogeschool's current locations
		
		sudo hcitool -i hci0 cmd 0x08 0x0008 1f 02 01 1a 1b ff ff ff be ac $uuid $floor 00 $HEX_CLASS c5 0$LOCATION
	fi
fi