# AltBeacon transmitter

AltBeacon transmitting script for a Digital Attendance Board project, within Zuyd Hogeschool. The execution of this project is done by Project Group 9, for minor BD05: Internet of Things.	


## Installation

For this project, a Linux system which can use Systemd services is required. along with a Bluetooth dongle (or an internal chip that has Bluetooth LE functionality). During the development, a Raspberry Pi 3 is used. For the Raspberry Pi systems, no Bluetooth dongle is required as their internal Bluetooth/Wi-Fi chip suffices.

Download the repository through your preferred method (cloning the Git repo or downloading the repo as a .zip archive).

From the repository folder, cut or copy the `AltBeacon.sh` file and paste it to a desired location. Keep the file path location in mind, for the `.service` file.

Open the `AltBeacon.service` file with your desired text editor and change the `ExecStart` variable to the file location where the `AltBeacon.sh` file is kept. By default, it points to the `pi` user's desktop. This is done, for the Raspberry Pi's user desktop while the user is still called `pi`. 

Also, replace the values after each parameter flag with the desired ones:

- -f flag represents the floor of the Beacon, in hexadecimal format (00 up to FF). For example: b3 represents wing and floor B3.
- -c represents the class as a decimal number. For example: 666 represents class 666.
- -u represents the UUIDv4 identifier for the Beacon, split up in pairs. It should have 32 characters in total, every two characters split up with a space. By default, it is all zeroes.
- -l represents the location of where the Beacon is. To represent the Zuyd Hogeschool locations as of now, it represents Sittard (1), Heerlen (2) and Maastricht (3).

Cut or copy the `AltBeacon.service` and paste the file to the `/etc/systemd/system` directory (with root permissions).

After placing the Systemd service file, open your terminal application and enter the following commands:

``` 
sudo systemctl daemon-reload 
sudo systemctl enable AltBeacon.service
```	
Reboot your Linux device, to let the AltBeacon service start the script. A few seconds after booting to the desktop, the system should be transmitting a Bluetooth LE signal representing the AltBeacon with the set-up details.

Keep in mind that the floor variable might not be displayed as hexadecimal, but as a decimal number, depending on which application you use for reading out the beacon data.

## Standalone Usage

To use the script as a standalone script, instead of a service booting up at reboot:

- Do *not* install the `.service` file as instructed prior.
- Use the following command to start the beacon, after replacing the dummied values:  

```
sh AltBeacon.sh -c <class number> -f <floor+wing hex> -u <UUIDv4> -l <number between 1 and 3>
```

## License
While the project is licensed with an [MIT license](https://gitlab.com/kevin_wijnen/altbeacon-transmitter/-/blob/main/LICENSE "MIT license for the project's own files"), the referenced file of Radius Networks for creating an AltBeacon-transmitting script is licensed under the [Apache 2.0 license](https://gitlab.com/kevin_wijnen/altbeacon-transmitter/-/blob/main/Radius%20Network%20LICENSE/LICENSE "Apache 2.0 license for the original referenced file, belonging to Radius Networks"). 

The licenses are available in the repository, with the Radius Networks license put into its own separate directory (Radius Networks LICENSE).